/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.edu.wat.wel.Threads;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.MulticastSocket;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.lang3.SerializationUtils;
import pl.edu.wat.wel.Radar.EntityController.RadarController;
import pl.edu.wat.wel.Simulation.Simulation;
import pl.edu.wat.wel.Simulation.SimulationEntity;
import pl.edu.wel.Fields.EntityID;
import pl.edu.wel.PDUs.CreateEntityPDU;
import pl.edu.wel.PDUs.EntityStatePDU;
import pl.edu.wel.PDUs.PDU;

/**
 *
 * @author Professional
 */
public class RadarThread extends Thread{
    
    private MulticastSocket socket;

    public RadarThread(MulticastSocket socket) {
        this.socket = socket;
    }

    public void run() {
        while (true) {
            byte[] buffer = new byte[5024];
            System.out.println("Radar Nasłuchuje");
            DatagramPacket packet = new DatagramPacket(buffer, buffer.length);

            try {
                socket.receive(packet);
            } catch (IOException ex) {
                Logger.getLogger(SocketThread.class.getName()).log(Level.SEVERE, null, ex);
            }
            PDU pdu = (PDU) SerializationUtils.deserialize(buffer);
            checkPduType(pdu, buffer);
        }
    }

    private void checkPduType(PDU pdu, byte[] buffer) {
        int type = pdu.getPduHeader().getPduType();
         if (type == 1) {
             RadarController rc = RadarController.getInstance();
            EntityStatePDU espdu = (EntityStatePDU) SerializationUtils.deserialize(buffer);


            rc.getRadarEntity().addToList(createSimulationEntity(espdu));

        }
    }

    private SimulationEntity createSimulationEntity(EntityStatePDU espdu) {
        SimulationEntity se = new SimulationEntity();
        se.setDeadReckoningParameters(espdu.getDeadReckoningParameters());
        se.setEntityAppearance(espdu.getEntityApperance());
        se.setEntityID(espdu.getEntityID());
        se.setEntityLinearVelocity(espdu.getEntityLinearVelocity());
        se.setEntityLocation(espdu.getEntityLocation());
        se.setEntityMarking(espdu.getEntityMarking());
        se.setEntityOrientation(espdu.getEntityOrientation());
        se.setEntityType(espdu.getEntityType());
        se.setForceID(espdu.getForceID());
        return se;
    }
}
