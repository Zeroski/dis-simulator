/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.edu.wat.wel.Radar.beans;

import pl.edu.wat.wel.Radar.EntityController.RadarController;
import pl.edu.wat.wel.Simulation.Simulation;
import pl.edu.wel.Fields.EntityLocation;

/**
 *
 * @author Professional
 */
public class LocationBean {

    private EntityLocation entityLocation;
    RadarController rc;

    private void addToSim() {
        rc = RadarController.getInstance();
        rc.getRadarEntity().setEntityLocation(entityLocation);
        rc.getRadarEntity().runReceiver();
    }

    public void createLocation(double x, double y, double z) {
        this.entityLocation = new EntityLocation();
        this.entityLocation.setxLComponent(x);
        this.entityLocation.setyLComponent(y);
        this.entityLocation.setzComponent(z);
        addToSim();
    }

    public EntityLocation getEntityLocation() {
        return entityLocation;
    }

    public void setEntityLocation(EntityLocation entityLocation) {
        this.entityLocation = entityLocation;
    }

}
//Utworzyć okno z informacjami o jednostce
//Algorytm Dead na póxniej