/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.edu.wat.wel.Radar.beans;

import java.util.ArrayList;
import java.util.List;
import pl.edu.wat.wel.Radar.EntityController.RadarController;
import pl.edu.wat.wel.Simulation.Simulation;
import pl.edu.wat.wel.entities.Enumerator;
import pl.edu.wel.Fields.EntityMarking;
import pl.edu.wat.wel.jdbc.OperacjeBazy;

/**
 *
 * @author Professional
 */
public class EntityMarkingBean {
    
    private EntityMarking entityMarking;
    private List<Enumerator> entityMarkingList;
    private OperacjeBazy operacje;
    RadarController rc;
    
    private void addToSim() {
        rc = RadarController.getInstance();
        rc.getRadarEntity().setEntityMarking(entityMarking);
    }
    
    public void createEntityMarking() {
        addToSim();
    }
    
    public void setMarking(String s) {
        this.entityMarking.setMark(s);
    }
    
    public void setEnumeratorCharacterSet(String s) {
        entityMarking = new EntityMarking();
        for (Enumerator e : entityMarkingList) {
            if (s.equals(e.getNazwa())) {
                this.entityMarking.setCharacterSet((byte)(int)e.getEnumerator());
            }
        }
    }
    public List<String> getEntityMarkingNames() {
        List<String> list = new ArrayList<>();
        for (Enumerator e : entityMarkingList) {
            list.add(e.getNazwa());
        }
        return list;
    }
    
    public EntityMarkingBean() {
        operacje = new OperacjeBazy();
        this.entityMarkingList = operacje.pokazCharacterSet();
    }

    public EntityMarking getEntityMarking() {
        return entityMarking;
    }

    public void setEntityMarking(EntityMarking entityMarking) {
        this.entityMarking = entityMarking;
    }

    public List<Enumerator> getEntityMarkingList() {
        return entityMarkingList;
    }

    public void setEntityMarkingList(List<Enumerator> entityMarkingList) {
        this.entityMarkingList = entityMarkingList;
    }
    
}
