/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.edu.wat.wel.Radar.beans;

import pl.edu.wat.wel.Radar.EntityController.RadarController;
import pl.edu.wel.Fields.EntityType;

/**
 *
 * @author Professional
 */
public class EntityTypeBean {
    
    private EntityType entityType;
    private RadarController rc;

    public EntityType getEntityType() {
        return entityType;
    }

    public void setEntityType(EntityType entityType) {
        this.entityType = entityType;
    }
    
    public void createEntityType() {
        entityType = new EntityType();
        entityType.setEntityKind((byte) 9);
        entityType.setDomain((byte) 1);
        entityType.setCountry((short) 225);
        entityType.setCategory((byte) 1);
        entityType.setSubcategory((byte) 24);
        entityType.setSpecific((byte) 2);
        entityType.setExtra((byte) 2);
        setEntityType(entityType);
        addToController();
    }
    private void addToController() {
        rc = RadarController.getInstance();
        rc.getRadarEntity().setEntityType(entityType);
    }
}
