/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.edu.wat.wel.Radar.beans;

import pl.edu.wat.wel.Radar.EntityController.RadarController;
import pl.edu.wat.wel.Simulation.Simulation;
import pl.edu.wel.Fields.EntityID;

/**
 *
 * @author Professional
 */
public class EntityIdBean {
    
    private EntityID entityId;
    private RadarController rc;
    private Simulation sim;

    public EntityID getEntityId() {
        return entityId;
    }

    public void setEntityId(EntityID entityId) {
        this.entityId = entityId;
    }
    
    public void createEntityId() {
        EntityID eid = new EntityID();
        sim = Simulation.getInstance();
        eid.setAppNumber(sim.getEntityId().getAppNumber());
        eid.setEntityNumber((short) Simulation.getNextEntityNumber());
        eid.setSiteNumber(sim.getEntityId().getSiteNumber());
        setEntityId(eid);
        addToController();
    }
    private void addToController() {
        rc = RadarController.getInstance();
        rc.getRadarEntity().setEntityID(entityId);
    }
}
