/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.edu.wat.wel.Radar.EntityController;

import pl.edu.wat.wel.Radar.entities.RadarEntity;
import pl.edu.wel.Fields.DeadReckoningParameters;
import pl.edu.wel.Fields.EntityLinearVelocity;
import pl.edu.wel.Fields.EntityOrientation;

/**
 *
 * @author Professional
 */
public class RadarController {
    
    private static RadarController radarController;
    private RadarEntity radarEntity;
    private static boolean radarCreated;

    public static boolean isRadarCreated() {
        return radarCreated;
    }

    public static void setRadarCreated(boolean radarCreated) {
        RadarController.radarCreated = radarCreated;
    }
    
    
    
    private RadarController() {
        radarEntity = new RadarEntity();
        radarEntity.setDeadReckoningParameters(new DeadReckoningParameters());
        radarEntity.setEntityAppearance(111111111);
        radarEntity.setEntityLinearVelocity(new EntityLinearVelocity());
        radarEntity.setEntityOrientation(new EntityOrientation());
    }
    
    public static RadarController getInstance() {
        if (radarController == null) {
            radarController = new RadarController();
        }
        return radarController;
    }

    public RadarEntity getRadarEntity() {
        return radarEntity;
    }

    public void setRadarEntity(RadarEntity simulationEntity) {
        this.radarEntity = simulationEntity;
    }
    
    
}
