/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.edu.wat.wel.Radar.entities;

import java.util.ArrayList;
import java.util.List;
import pl.edu.wat.wel.Simulation.SimulationEntity;
import pl.edu.wat.wel.Threads.RadarThread;
import pl.edu.wel.Fields.EntityID;

/**
 *
 * @author Professional
 */
public class RadarEntity extends SimulationEntity{
    
    private List<SimulationEntity> detectedEntities;
    
    
    public ArrayList<String> getEntitiesNames() {
        ArrayList<String> list = new ArrayList<>();
        for (SimulationEntity detectedEntity : detectedEntities) {
            String s = detectedEntity.getMarkingWithId();
            list.add(s);
        }
        return list;
    }
    
    public SimulationEntity searchInList(EntityID eid) {
        for (SimulationEntity se : detectedEntities) {
            if (eid.equals(se.getEntityID())) {
                return se;
            }
        }
        return null;
    }
    public RadarEntity() {
        super();
        detectedEntities = new ArrayList<>();
    }
    public void runReceiver() {
        super.setSocketReceiver();
        new RadarThread(super.getSocketReceiver()).start();
    }

    public void updateList(SimulationEntity se) {
        for (pl.edu.wat.wel.Simulation.SimulationEntity sEntity : detectedEntities) {
            if (se.getEntityID().equals(sEntity.getEntityID())) {
                sEntity = se;
            }
        }
    }
    
    public void addToList(SimulationEntity se) {
        if (!entityExist(se)) {
            System.out.println("Dodano do detected");
            detectedEntities.add(se);
        }
        else {
            System.out.println("updateuje ddetected");
            updateList(se);
        }
    }
    private boolean entityExist(SimulationEntity se) {
        for (SimulationEntity sEntity : detectedEntities) {
            if (se.getEntityID().equals(sEntity.getEntityID())) {

                return true;
            }
            
        }
        return false;
    }
    
    
    public List<SimulationEntity> getDetectedEntities() {
        return detectedEntities;
    }

    public void setDetectedEntities(List<SimulationEntity> detectedEntities) {
        this.detectedEntities = detectedEntities;
    }
    
    
}
