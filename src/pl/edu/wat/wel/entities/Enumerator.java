/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.edu.wat.wel.entities;

/**
 *
 * @author Professional
 */
public class Enumerator {
    
    private Integer id;
    private Integer enumerator;
    private String nazwa;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getEnumerator() {
        return enumerator;
    }

    public void setEnumerator(Integer enumerator) {
        this.enumerator = enumerator;
    }

    public String getNazwa() {
        return nazwa;
    }

    public void setNazwa(String nazwa) {
        this.nazwa = nazwa;
    }
    
    
}
