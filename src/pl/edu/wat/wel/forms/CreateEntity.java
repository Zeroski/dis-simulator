/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.edu.wat.wel.forms;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import pl.edu.wat.wel.creators.EntityTypeCreator;
import pl.edu.wat.wel.entities.Enumerator;
import pl.edu.wat.wel.jdbc.OperacjeBazy;

/**
 *
 * @author Professional
 */
public class CreateEntity extends javax.swing.JFrame {

    /**
     * Creates new form CreateEntityTest
     */
    private OperacjeBazy operacje;
    private EntityTypeCreator entityTypeCreator;

    public CreateEntity() {
        initComponents();
        operacje = new OperacjeBazy();
        entityTypeCreator = new EntityTypeCreator();
        initComboBox();

    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jComboBox1 = new javax.swing.JComboBox();
        jButton1 = new javax.swing.JButton();
        jComboBox2 = new javax.swing.JComboBox();
        jButton2 = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        jComboBox3 = new javax.swing.JComboBox();
        jButton3 = new javax.swing.JButton();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jComboBox4 = new javax.swing.JComboBox();
        jButton4 = new javax.swing.JButton();
        jLabel4 = new javax.swing.JLabel();
        jComboBox5 = new javax.swing.JComboBox();
        jButton5 = new javax.swing.JButton();
        jLabel5 = new javax.swing.JLabel();
        jComboBox6 = new javax.swing.JComboBox();
        jButton6 = new javax.swing.JButton();
        jLabel6 = new javax.swing.JLabel();
        jComboBox7 = new javax.swing.JComboBox();
        jButton7 = new javax.swing.JButton();
        jLabel7 = new javax.swing.JLabel();
        jButton8 = new javax.swing.JButton();
        jButton9 = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Entity Type");

        jButton1.setText("Potwierdź");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        jComboBox2.setEnabled(false);

        jButton2.setText("Potwirdź");
        jButton2.setEnabled(false);
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });

        jLabel1.setText("Entity Kind: ");

        jComboBox3.setEnabled(false);

        jButton3.setText("Potwierdź");
        jButton3.setEnabled(false);
        jButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton3ActionPerformed(evt);
            }
        });

        jLabel2.setText("Domain: ");

        jLabel3.setText("Country: ");

        jComboBox4.setEnabled(false);

        jButton4.setText("Potwierdź");
        jButton4.setEnabled(false);
        jButton4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton4ActionPerformed(evt);
            }
        });

        jLabel4.setText("Category: ");

        jComboBox5.setEnabled(false);

        jButton5.setText("Potwierdź");
        jButton5.setEnabled(false);
        jButton5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton5ActionPerformed(evt);
            }
        });

        jLabel5.setText("Subcategory:");

        jComboBox6.setEnabled(false);

        jButton6.setText("Potwierdź");
        jButton6.setEnabled(false);
        jButton6.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton6ActionPerformed(evt);
            }
        });

        jLabel6.setText("Specific: ");

        jComboBox7.setEnabled(false);

        jButton7.setText("Potwierdź");
        jButton7.setEnabled(false);
        jButton7.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton7ActionPerformed(evt);
            }
        });

        jLabel7.setText("Extra: ");

        jButton8.setText("Resetuj dane");
        jButton8.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton8ActionPerformed(evt);
            }
        });

        jButton9.setText("Zatwierdź Entity Type");
        jButton9.setEnabled(false);
        jButton9.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton9ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(jComboBox7, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jComboBox6, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jComboBox5, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jComboBox4, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jComboBox3, 0, 170, Short.MAX_VALUE)
                            .addComponent(jComboBox1, javax.swing.GroupLayout.Alignment.LEADING, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jComboBox2, javax.swing.GroupLayout.Alignment.LEADING, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jButton2, javax.swing.GroupLayout.PREFERRED_SIZE, 79, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jLabel2))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jButton1)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jLabel1))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jButton3)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jLabel3))
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                    .addComponent(jButton5, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(jButton4, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel4)
                                    .addComponent(jLabel5)))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jButton6)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jLabel6))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jButton7)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jLabel7))))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jButton8)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jButton9)))
                .addContainerGap(166, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jComboBox1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton1)
                    .addComponent(jLabel1))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jComboBox2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton2)
                    .addComponent(jLabel2))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jComboBox3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton3)
                    .addComponent(jLabel3))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jComboBox4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton4)
                    .addComponent(jLabel4))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jComboBox5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton5)
                    .addComponent(jLabel5))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jComboBox6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton6)
                    .addComponent(jLabel6))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jComboBox7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton7)
                    .addComponent(jLabel7))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 30, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jButton8)
                    .addComponent(jButton9))
                .addGap(24, 24, 24))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        // TODO add your handling code here:
        updateEntityKind();
        refreshLabels();

    }//GEN-LAST:event_jButton1ActionPerformed

    private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton3ActionPerformed
        // TODO add your handling code here:
        updateCountry();
        refreshLabels();
    }//GEN-LAST:event_jButton3ActionPerformed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        // TODO add your handling code here:
        updateDomain();
        refreshLabels();
    }//GEN-LAST:event_jButton2ActionPerformed

    private void jButton5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton5ActionPerformed
        // TODO add your handling code here:
        updateSubcategory();
        refreshLabels();
    }//GEN-LAST:event_jButton5ActionPerformed

    private void jButton4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton4ActionPerformed
        // TODO add your handling code here:
        updateCategory();
        refreshLabels();
    }//GEN-LAST:event_jButton4ActionPerformed

    private void jButton6ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton6ActionPerformed
        // TODO add your handling code here:
        updateSpecific();
        refreshLabels();
    }//GEN-LAST:event_jButton6ActionPerformed

    private void jButton7ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton7ActionPerformed
        // TODO add your handling code here:
        updateExtra();
        refreshLabels();
    }//GEN-LAST:event_jButton7ActionPerformed

    private void jButton9ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton9ActionPerformed
        // TODO add your handling code here:
        entityTypeCreator.createEntiyType();
        this.dispose();
        new EntityIdForm().setVisible(true);
        //zamknac okno dac kolejne ustawiajace lokalizacje
    }//GEN-LAST:event_jButton9ActionPerformed

    private void jButton8ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton8ActionPerformed
        // TODO add your handling code here:
        resetuj();

    }//GEN-LAST:event_jButton8ActionPerformed

    /**
     * @param args the command line arguments
     */
//    public static void main(String args[]) {
//        /* Set the Nimbus look and feel */
//        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
//        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
//         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
//         */
//        try {
//            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
//                if ("Nimbus".equals(info.getName())) {
//                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
//                    break;
//                }
//            }
//        } catch (ClassNotFoundException ex) {
//            java.util.logging.Logger.getLogger(CreateEntity.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        } catch (InstantiationException ex) {
//            java.util.logging.Logger.getLogger(CreateEntity.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        } catch (IllegalAccessException ex) {
//            java.util.logging.Logger.getLogger(CreateEntity.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
//            java.util.logging.Logger.getLogger(CreateEntity.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        }
//        //</editor-fold>
//        //</editor-fold>
//
//        /* Create and display the form */
//        java.awt.EventQueue.invokeLater(new Runnable() {
//            public void run() {
//                new CreateEntity().setVisible(true);
//            }
//        });
//    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton3;
    private javax.swing.JButton jButton4;
    private javax.swing.JButton jButton5;
    private javax.swing.JButton jButton6;
    private javax.swing.JButton jButton7;
    private javax.swing.JButton jButton8;
    private javax.swing.JButton jButton9;
    private javax.swing.JComboBox jComboBox1;
    private javax.swing.JComboBox jComboBox2;
    private javax.swing.JComboBox jComboBox3;
    private javax.swing.JComboBox jComboBox4;
    private javax.swing.JComboBox jComboBox5;
    private javax.swing.JComboBox jComboBox6;
    private javax.swing.JComboBox jComboBox7;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    // End of variables declaration//GEN-END:variables
    private void initComboBox() {
        jComboBox1.setModel(new DefaultComboBoxModel(entityTypeCreator.getEKNames().toArray()));

    }

    private void updateEntityKind() {
        String selected = jComboBox1.getSelectedItem().toString();
        entityTypeCreator.setEnumeratorEK(selected);
        if (jComboBox1.getSelectedIndex() == 0) {
            setEnableSettings(jComboBox2, jButton2, false);
            jButton9.setEnabled(true);
            setEnableSettings(jComboBox3, jButton3, false);
        } else {
            entityTypeCreator.setListDomain();
            jComboBox2.setModel(new DefaultComboBoxModel(entityTypeCreator.getDomainNames().toArray()));
            setEnableSettings(jComboBox2, jButton2, true);
        }
        setEnableSettings(jComboBox1, jButton1, false);
    }

    private void updateDomain() {
        String selected = jComboBox2.getSelectedItem().toString();
        entityTypeCreator.setEnumeratorDomain(selected);
        if (jComboBox2.getSelectedIndex() == 0) {
            setEnableSettings(jComboBox4, jButton4, false);
            jButton9.setEnabled(true);
            setEnableSettings(jComboBox3, jButton3, false);
        } else {
            entityTypeCreator.setListCategory();
            entityTypeCreator.setListCountry();
            if (entityTypeCreator.getListCountry() != null) {
                jComboBox3.setModel(new DefaultComboBoxModel(entityTypeCreator.getCountryNames().toArray()));
                setEnableSettings(jComboBox3, jButton3, true);             
            }
            if (entityTypeCreator.getCategoryNames() != null) {
                jComboBox4.setModel(new DefaultComboBoxModel(entityTypeCreator.getCategoryNames().toArray()));
                setEnableSettings(jComboBox4, jButton4, true);
            } else {
                setEnableSettings(jComboBox4, jButton4, false);
            }
            if (entityTypeCreator.getListCategory() == null && entityTypeCreator.getListCountry() == null) 
                jButton9.setEnabled(true);
        }
        setEnableSettings(jComboBox2, jButton2, false);

    }

    //najlepiej gdyby na labelach wyskakiwaly dane z ET obiektu update- dodac metode ktora odsiweza po kazdym guziku
    private void updateCountry() {
        String selected = jComboBox3.getSelectedItem().toString();
        entityTypeCreator.setEnumeratorCountry(selected);
        setEnableSettings(jComboBox3, jButton3, false);

        if (entityTypeCreator.getCategoryNames() != null) {
            jComboBox4.setModel(new DefaultComboBoxModel(entityTypeCreator.getCategoryNames().toArray()));
            setEnableSettings(jComboBox4, jButton4, true);
        } else {
            setEnableSettings(jComboBox4, jButton4, false);
        }
    }

    private void updateCategory() {
        String selected = jComboBox4.getSelectedItem().toString();
        entityTypeCreator.setEnumeratorCategory(selected);
        entityTypeCreator.setListSubcategory();
        if (entityTypeCreator.getSubcategoryNames() == null) {
            setEnableSettings(jComboBox5, jButton5, false);
            jButton9.setEnabled(true);
        } else {
            jComboBox5.setModel(new DefaultComboBoxModel(entityTypeCreator.getSubcategoryNames().toArray()));
            setEnableSettings(jComboBox5, jButton5, true);
        }
        setEnableSettings(jComboBox4, jButton4, false);
    }

    private void updateSubcategory() {
        String selected = jComboBox5.getSelectedItem().toString();
        entityTypeCreator.setEnumeratorSubcategory(selected);
        entityTypeCreator.setListSpecific();
        if (entityTypeCreator.getSpecificNames() == null) {
            setEnableSettings(jComboBox6, jButton6, false);
            jButton9.setEnabled(true);
        } else {
            jComboBox6.setModel(new DefaultComboBoxModel(entityTypeCreator.getSpecificNames().toArray()));
            setEnableSettings(jComboBox6, jButton6, true);
        }
        setEnableSettings(jComboBox5, jButton5, false);
    }

    private void updateSpecific() {
        String selected = jComboBox6.getSelectedItem().toString();
        entityTypeCreator.setEnumeratorSpecific(selected);
        setEnableSettings(jComboBox6, jButton6, false);
        enableExtra();
    }

    private void updateExtra() {
        String selected = jComboBox7.getSelectedItem().toString();
        entityTypeCreator.setEnumeratorExtra(selected);
        setEnableSettings(jComboBox7, jButton7, false);
        jButton9.setEnabled(true);
    }

    private void enableExtra() {
        setEnableSettings(jComboBox7, jButton7, true);
        entityTypeCreator.setListExtra();
        if (entityTypeCreator.getExtraNames() == null) {
            setEnableSettings(jComboBox7, jButton7, false);
        } else {
            jComboBox7.setModel(new DefaultComboBoxModel(entityTypeCreator.getExtraNames().toArray()));
            setEnableSettings(jComboBox7, jButton7, true);
        }
    }

    private void clearLabels() {
        jLabel1.setText("Entity Kind: ");
        jLabel2.setText("Domain: ");
        jLabel3.setText("Country: ");
        jLabel4.setText("Category: ");
        jLabel5.setText("Subcategory: ");
        jLabel6.setText("Specific: ");
        jLabel7.setText("Extra: ");
    }

    private void refreshLabels() {
        StringBuilder sb = new StringBuilder("Entity Kind: ");
        if (entityTypeCreator.getEnumeratorEK() != null) {
            sb.append(entityTypeCreator.getEnumeratorEK().getNazwa());
            jLabel1.setText(sb.toString());
        }
        sb = new StringBuilder("Domain: ");
        if (entityTypeCreator.getEnumeratorDomain() != null) {
            sb.append(entityTypeCreator.getEnumeratorDomain().getNazwa());
            jLabel2.setText(sb.toString());
        }
        sb = new StringBuilder("Conutry: ");
        if (entityTypeCreator.getEnumeratorCountry() != null) {
            sb.append(entityTypeCreator.getEnumeratorCountry().getNazwa());
            jLabel3.setText(sb.toString());
        }
        sb = new StringBuilder("Category: ");
        if (entityTypeCreator.getEnumeratorCategory() != null) {
            sb.append(entityTypeCreator.getEnumeratorCategory().getNazwa());
            jLabel4.setText(sb.toString());
        }
        sb = new StringBuilder("Subcategory: ");
        if (entityTypeCreator.getEnumeratorSubcategory() != null) {
            sb.append(entityTypeCreator.getEnumeratorSubcategory().getNazwa());
            jLabel5.setText(sb.toString());
        }
        sb = new StringBuilder("Specific: ");
        if (entityTypeCreator.getEnumeratorSpecific() != null) {
            System.out.println("Enumerator Specific: " + entityTypeCreator.getEnumeratorSpecific().getNazwa());
            sb.append(entityTypeCreator.getEnumeratorSpecific().getNazwa());
            jLabel6.setText(sb.toString());
        }
        sb = new StringBuilder("Extra: ");
        if (entityTypeCreator.getEnumeratorExtra() != null) {
            sb.append(entityTypeCreator.getEnumeratorExtra().getNazwa());
            jLabel7.setText(sb.toString());
        }

    }

    private void resetuj() {
        entityTypeCreator = new EntityTypeCreator();
        cleanJCB();
        setEnableSettings(jComboBox1, jButton1, true);
        setEnableSettings(jComboBox2, jButton2, false);
        setEnableSettings(jComboBox3, jButton3, false);
        setEnableSettings(jComboBox4, jButton4, false);
        setEnableSettings(jComboBox5, jButton5, false);
        setEnableSettings(jComboBox6, jButton6, false);
        setEnableSettings(jComboBox7, jButton7, false);
        jButton9.setEnabled(false);
        clearLabels();

    }

    private void setEnableSettings(JComboBox jcb, JButton jb, boolean b) {
        jcb.setEnabled(b);
        jb.setEnabled(b);
    }

    private void cleanJCB() {
        jComboBox2.removeAllItems();
        jComboBox3.removeAllItems();
        jComboBox4.removeAllItems();
        jComboBox5.removeAllItems();
        jComboBox6.removeAllItems();
        jComboBox7.removeAllItems();
    }
}


