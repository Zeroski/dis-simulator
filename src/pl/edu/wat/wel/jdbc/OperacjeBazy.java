/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.edu.wat.wel.jdbc;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import pl.edu.wat.wel.entities.Enumerator;

/**
 *
 * @author Professional
 */
public class OperacjeBazy {

    private static final String url = "jdbc:postgresql://localhost:5432/DIS_Library";
    private static final String username = "DisUser";
    private static final String password = "password";

    private Connection connection = null;

    public OperacjeBazy() {
        getConnection();
    }

    private void getConnection() {
        try {
            connection = DriverManager.getConnection(url, username, password);
        } catch (SQLException ex) {
            System.out.println("Problem z połączeniem do bazy");
            Logger.getLogger(OperacjeBazy.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    //tworzenie Entity Type
    public List<Enumerator> pokazWszystkieEntityKind() {
        PreparedStatement preparedStatement = null;
        ResultSet rs = null;
        String query = "SELECT id, nazwa, enumerator FROM Enumerator WHERE nazwaenumeratora='Entity Kind'";
        Enumerator enumerator;
        List<Enumerator> list = new ArrayList<>();
        try {
            preparedStatement = connection.prepareStatement(query);
            rs = preparedStatement.executeQuery();
            while (rs.next()) {
                enumerator = new Enumerator();
                enumerator.setId(rs.getInt("ID"));
                enumerator.setEnumerator(rs.getInt("ENUMERATOR"));
                enumerator.setNazwa(rs.getString("NAZWA"));
                list.add(enumerator);
            }
        } catch (SQLException ex) {
            Logger.getLogger(OperacjeBazy.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                rs.close();
                preparedStatement.close();
            } catch (SQLException ex) {
                Logger.getLogger(OperacjeBazy.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return list;
    }

    public List<Enumerator> pokazDomeny(Integer entityKindID) {
        System.out.println("EnumID: " + entityKindID);
        PreparedStatement preparedStatement = null;
        ResultSet rs = null;
        String query = null;
        Enumerator enumerator;
        List list = null;
        try {
            System.out.println("===========Domeny==============");

            if (entityKindID == 11) {
                query = "SELECT id, nazwa, enumerator FROM Enumerator WHERE nazwaenumeratora='Munition domain'";
            } else if (entityKindID == 9) {
                System.out.println("Zablokuj");
            } else {
                query = "SELECT id, nazwa, enumerator FROM Enumerator WHERE nazwaenumeratora='Platform domain'";
            }
            if (query != null) {
                list = new ArrayList();
                preparedStatement = connection.prepareStatement(query);
                rs = preparedStatement.executeQuery();

                while (rs.next()) {
                    enumerator = new Enumerator();
                    enumerator.setId(rs.getInt(1));
                    enumerator.setNazwa(rs.getString(2));
                    enumerator.setEnumerator(rs.getInt(3));
                    list.add(enumerator);
                }
            }
        } catch (SQLException ex) {

            Logger.getLogger(OperacjeBazy.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                if (query != null) {
                    rs.close();
                    preparedStatement.close();
                }
            } catch (SQLException ex) {
                Logger.getLogger(OperacjeBazy.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return list;
    }

    public List<Enumerator> pokazKraje(Integer idEntityKind) {
        PreparedStatement preparedStatement = null;
        ResultSet rs = null;
        String query = null;
        if ((idEntityKind == 10 || idEntityKind == 12)) 
        query = "SELECT id, nazwa, enumerator FROM Enumerator WHERE nazwaenumeratora='Country'";
        List<Enumerator> list = null;
        Enumerator enumerator;
        try {
            if (query != null) {
                list = new ArrayList();
                preparedStatement = connection.prepareCall(query);
                rs = preparedStatement.executeQuery();
                while (rs.next()) {
                    enumerator = new Enumerator();
                    enumerator.setId(rs.getInt("ID"));
                    enumerator.setEnumerator(rs.getInt("Enumerator"));
                    enumerator.setNazwa(rs.getString("Nazwa"));
                    list.add(enumerator);
                }
            }
        } catch (SQLException ex) {
            Logger.getLogger(OperacjeBazy.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                if (query != null) {
                    rs.close();
                    preparedStatement.close();
                }
            } catch (SQLException ex) {
                Logger.getLogger(OperacjeBazy.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return list;
    }

    public List<Enumerator> pokazKategorie(Integer idEntityKind, Integer idDomain) {
        PreparedStatement preparedStatement = null;
        ResultSet rs = null;
        String query = null;
        List<Enumerator> list = null;
        Enumerator enumerator;
        if (idEntityKind == 9 || idDomain == 14 || idDomain == 20) {
            System.out.println("Błąd: Niepoprawnie skonfigurowane tworzenie ETntitytype");
        } else if (idEntityKind == 10) {
            switch (idDomain) {
                case 15:
                    query = "SELECT id, nazwa , enumerator FROM Enumerator WHERE nazwaenumeratora='Platform Land Category'";
                    break;
                case 16:
                    query = "SELECT id, nazwa , enumerator FROM Enumerator WHERE nazwaenumeratora='Platform Air Category'";
                    break;
                case 17:
                    query = "SELECT id, nazwa , enumerator FROM Enumerator WHERE nazwaenumeratora='Platform Surface Category'";
                    break;
                case 18:
                    query = "SELECT id, nazwa , enumerator FROM Enumerator WHERE nazwaenumeratora='Platform Subsurface Category'";
                    break;
                case 19:
                    query = "SELECT id, nazwa , enumerator FROM Enumerator WHERE nazwaenumeratora='Platform Space Category'";
                    break;
            }
        } else if (idEntityKind == 11) {
            query = "SELECT id, nazwa, enumerator FROM Enumerator WHERE nazwaenumeratora='Munition Category'";
        } else if (idEntityKind == 12) {
            switch (idDomain) {
                case 15:
                    query = "SELECT id, nazwa , enumerator FROM Enumerator WHERE nazwaenumeratora='Life Form Land Category'";
                    break;
                case 16:
                    query = "SELECT id, nazwa , enumerator FROM Enumerator WHERE nazwaenumeratora='Life Form Air Category'";
                    break;
                case 18:
                    query = "SELECT id, nazwa , enumerator FROM Enumerator WHERE nazwaenumeratora='Life Form Subsurface Category'";
                    break;
            }
        }
        try {
            System.out.println("=========Kategorie============");
            if (query != null) {
                list = new ArrayList();
                preparedStatement = connection.prepareStatement(query);

                rs = preparedStatement.executeQuery();
                while (rs.next()) {
                    enumerator = new Enumerator();
                    enumerator.setId(rs.getInt("ID"));
                    enumerator.setEnumerator(rs.getInt("Enumerator"));
                    enumerator.setNazwa(rs.getString("Nazwa"));
                    list.add(enumerator);
                }
            }
        } catch (SQLException ex) {
            Logger.getLogger(OperacjeBazy.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                if (query != null) {
                    rs.close();
                    preparedStatement.close();
                }
            } catch (SQLException ex) {
                Logger.getLogger(OperacjeBazy.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return list;
    }

    public List<Enumerator> pokazPodkategorie(Integer idEntityKind, Integer idDomain, Integer idCategory) {
        PreparedStatement preparedStatement = null;
        ResultSet rs = null;
        String query = null;
        List<Enumerator> list = null;
        System.out.println("EK: " + idEntityKind);
        System.out.println("Domain: " + idDomain);
        System.out.println("Category: " + idCategory);
        switch (idEntityKind) {
            case 10:
                switch (idDomain) {
                    case 15:
                        switch (idCategory) {
                            case 98:
                                query = "SELECT id, nazwa, enumerator FROM Enumerator WHERE nazwaenumeratora='Platform Land Motorcycle Subcategory'";
                                break;
                            case 99:
                                query = "SELECT id, nazwa, enumerator FROM Enumerator WHERE nazwaenumeratora='Platform Land Car Subcategory'";
                                break;
                        }
                        break;
                    case 16:
                        if (idCategory == 103) {
                            query = "SELECT id, nazwa, enumerator FROM Enumerator WHERE nazwaenumeratora='Platform Air Aircraft Subcategory'";
                        }
                        break;
                }
                break;
            case 12:
                switch (idDomain) {
                    case 15:
                        if (idCategory >= 116 && idCategory <= 118) {
                            query = "SELECT id, nazwa, enumerator FROM Enumerator WHERE nazwaenumeratora='Life Form Land Humans Subcategory'";
                        }
                        break;
                }
                break;

        }
        try {
            if (query != null) {
                list = new ArrayList();
                preparedStatement = connection.prepareStatement(query);

                rs = preparedStatement.executeQuery();
                list = setEnumeratorList(rs);
            }
        } catch (SQLException ex) {
            Logger.getLogger(OperacjeBazy.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                if (query != null) {
                    rs.close();
                    preparedStatement.close();
                }
            } catch (SQLException ex) {
                Logger.getLogger(OperacjeBazy.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return list;
    }

    public List<Enumerator> pokazSpecyfikacje(Integer idSubcategory) {
        PreparedStatement preparedStatement = null;
        ResultSet rs = null;
        List<Enumerator> list = null;
        Enumerator enumerator = null;
        String query = null;
        switch (idSubcategory) {
            case 131:
                query = "SELECT id, nazwa, enumerator FROM enumerator WHERE nazwaenumeratora='Life Form Land Humans Assault Specific'";
                break;
            case 132:
                query = "SELECT id, nazwa, enumerator FROM enumerator WHERE nazwaenumeratora='Life Form Land Humans Sub Machine Guns'";
                break;
        }
        try {
            if (query != null) {
                list = new ArrayList();
                preparedStatement = connection.prepareStatement(query);
                rs = preparedStatement.executeQuery();
                list = setEnumeratorList(rs);
            }

        } catch (SQLException ex) {
            Logger.getLogger(OperacjeBazy.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                if (query != null) {
                    rs.close();
                    preparedStatement.close();
                }
            } catch (SQLException ex) {
                Logger.getLogger(OperacjeBazy.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return list;

    }

    public List<Enumerator> pokazExtra(Integer idCategory) {
        PreparedStatement preparedStatement = null;
        ResultSet rs = null;
        List<Enumerator> list = null;
        Enumerator enumerator = null;
        String query = null;
        if (idCategory >= 116 && idCategory <= 118) {
            query = "SELECT id, nazwa, enumerator FROM Enumerator WHERE nazwaenumeratora='Life Form Land Humans Extra'";
        }
        try {
            if (query != null) {
                list = new ArrayList();
                preparedStatement = connection.prepareStatement(query);
                rs = preparedStatement.executeQuery();
                list = setEnumeratorList(rs);
            }

        } catch (SQLException ex) {
            Logger.getLogger(OperacjeBazy.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                if (query != null) {
                    rs.close();
                    preparedStatement.close();
                }
            } catch (SQLException ex) {
                Logger.getLogger(OperacjeBazy.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return list;

    }
    
    public List<Enumerator> pokazForceId() {
        PreparedStatement preparedStatement = null;
        ResultSet rs = null;
        List<Enumerator> list = null;
        Enumerator enumerator = null;
        String query = null;
        
            query = "SELECT id, nazwa, enumerator FROM Enumerator WHERE nazwaenumeratora='Force ID'";
        
        try {
            if (query != null) {
                list = new ArrayList();
                preparedStatement = connection.prepareStatement(query);
                rs = preparedStatement.executeQuery();
                list = setEnumeratorList(rs);
            }

        } catch (SQLException ex) {
            Logger.getLogger(OperacjeBazy.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                if (query != null) {
                    rs.close();
                    preparedStatement.close();
                }
            } catch (SQLException ex) {
                Logger.getLogger(OperacjeBazy.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return list;

    }
    
    public List<Enumerator> pokazCharacterSet() {
        PreparedStatement preparedStatement = null;
        ResultSet rs = null;
        List<Enumerator> list = null;
        Enumerator enumerator = null;
        String query = null;
        
            query = "SELECT id, nazwa, enumerator FROM Enumerator WHERE nazwaenumeratora='Character Set'";
        
        try {
            if (query != null) {
                list = new ArrayList();
                preparedStatement = connection.prepareStatement(query);
                rs = preparedStatement.executeQuery();
                list = setEnumeratorList(rs);
            }

        } catch (SQLException ex) {
            Logger.getLogger(OperacjeBazy.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                if (query != null) {
                    rs.close();
                    preparedStatement.close();
                }
            } catch (SQLException ex) {
                Logger.getLogger(OperacjeBazy.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return list;

    }

    private List<Enumerator> setEnumeratorList(ResultSet rs) {
        Enumerator enumerator;
        List list = new ArrayList();

        try {

            while (rs.next()) {

                enumerator = new Enumerator();
                enumerator.setId(rs.getInt("ID"));
                enumerator.setEnumerator(rs.getInt("Enumerator"));
                enumerator.setNazwa(rs.getString("Nazwa"));

                list.add(enumerator);
            }
        } catch (SQLException ex) {
            Logger.getLogger(OperacjeBazy.class.getName()).log(Level.SEVERE, null, ex);
            throw new RuntimeException();
        }
        return list;
    }
}
