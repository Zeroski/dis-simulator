/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.edu.wat.wel.jdbc;

import java.util.List;
import pl.edu.wat.wel.entities.Enumerator;

/**
 *
 * @author Professional
 */
public class TestClass {
    
    public static void main(String[] args) {
        
        int idDomain = 18;
        int idEntityKind = 12;
        int idSubcategory = 130;
        
        OperacjeBazy operacje = new OperacjeBazy();
        
        List<Enumerator> list = operacje.pokazSpecyfikacje(idSubcategory);
        
        for (Enumerator e : list) {
            System.out.println("ID: " + e.getId());
            System.out.println("Nazwa: " + e.getNazwa());
            System.out.println("Enumerator " + e.getEnumerator());
        }
    }
}
