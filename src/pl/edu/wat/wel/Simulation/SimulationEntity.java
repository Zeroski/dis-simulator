/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.edu.wat.wel.Simulation;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.InetAddress;
import java.net.MulticastSocket;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.lang3.SerializationUtils;
import pl.edu.wel.Fields.DeadReckoningParameters;
import pl.edu.wel.Fields.EntityID;
import pl.edu.wel.Fields.EntityLinearVelocity;
import pl.edu.wel.Fields.EntityLocation;
import pl.edu.wel.Fields.EntityMarking;
import pl.edu.wel.Fields.EntityOrientation;
import pl.edu.wel.Fields.EntityType;
import pl.edu.wel.Fields.PDUHeader;
import pl.edu.wel.PDUs.CreateEntityPDU;
import pl.edu.wel.PDUs.EntityStatePDU;

/**
 *
 * @author Professional
 */
public class SimulationEntity {
    
    private EntityLocation entityLocation;
    private EntityType entityType;
    private EntityID entityID;
    private byte forceID;
    private EntityLinearVelocity entityLinearVelocity;
    private EntityOrientation entityOrientation;
    private Integer entityAppearance;
    private DeadReckoningParameters deadReckoningParameters;
    private EntityMarking entityMarking;
    private Simulation sim;
    private MulticastSocket socketSender;
    private InetAddress adress;
    private MulticastSocket socketReceiver;

    protected MulticastSocket getSocketReceiver() {
        return socketReceiver;
    }

    protected void setSocketReceiver(MulticastSocket socketReceiver) {
        this.socketReceiver = socketReceiver;
    }
    
    
    
    
    
    public String getMarkingWithId() {
        StringBuilder sb = new StringBuilder();
        sb.append(entityMarking.getMark());
        sb.append(": ");
        sb.append(entityID.getEntityNumber());
        return sb.toString();
    }
    
    protected void setSocketReceiver() {
        if (socketReceiver == null) {
            try {
                socketReceiver = new MulticastSocket(Simulation.getPORT());
                adress = InetAddress.getByName(Simulation.getIpAdress());
                socketReceiver.joinGroup(adress);
            } catch (IOException ex) {
                Logger.getLogger(SimulationEntity.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    
    private void setSocketSender() {
        if (socketSender == null) {
            try {
                socketSender = new MulticastSocket(Simulation.getPORT());
                adress = InetAddress.getByName(Simulation.getIpAdress());
                socketSender.joinGroup(adress);
            } catch (IOException ex) {
                Logger.getLogger(SimulationEntity.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    
    public void sendEntityStatePdu() {
        setSocketSender();
        EntityStatePDU espdu = new EntityStatePDU();
        espdu.setAlternateEntityType(new EntityType());
        espdu.setDeadReckoningParameters(new DeadReckoningParameters());
        espdu.setEntityApperance(entityAppearance);
        espdu.setEntityID(entityID);
        espdu.setEntityLinearVelocity(entityLinearVelocity);
        espdu.setEntityLocation(entityLocation);
        espdu.setEntityMarking(entityMarking);
        espdu.setEntityOrientation(entityOrientation);
        espdu.setEntityType(entityType);
        espdu.setForceID(forceID);
        espdu.setNoVPR((byte) 0);
        espdu.setPduHeader(createPduHeader(1, 1));
        //1577
        byte[] buffer = SerializationUtils.serialize(espdu);
        
        DatagramPacket packet = new DatagramPacket(buffer, buffer.length, adress, Simulation.getPORT());
        
        try {
            socketSender.send(packet);
        } catch (IOException ex) {
            Logger.getLogger(SimulationEntity.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void sendCreateEntityPdu() {
        setSocketSender();
        sim = Simulation.getInstance();
        CreateEntityPDU cepdu = new CreateEntityPDU();
        cepdu.setPduHeader(createPduHeader(11, 5));
        cepdu.setOrginatingId(entityID);
        cepdu.setReceivingId(sim.getEntityId());
        int next = Simulation.getActualRequestId();
        Simulation.setActualIdForEntity(next + 1);
        cepdu.setRequestId(next);
        //435
        byte[] buffer = SerializationUtils.serialize(cepdu);
        
        DatagramPacket packet = new DatagramPacket(buffer, buffer.length, adress, Simulation.getPORT());
        try {
            socketSender.send(packet);
        } catch (IOException ex) {
            Logger.getLogger(SimulationEntity.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    private PDUHeader createPduHeader(int pduType, int pduFamily) {
        sim = Simulation.getInstance();
        PDUHeader pduHeader = new PDUHeader();
        pduHeader.setExerciseID(sim.getExerciseID());
        pduHeader.setLength((short) 1154);
        pduHeader.setPadding((byte) 0);
        pduHeader.setPduStatus((byte) 0);
        pduHeader.setPduType((byte) pduType);
        pduHeader.setProtocolFamily((byte) pduFamily);
        pduHeader.setTimestamp(0);
        pduHeader.setProtocolVersion((byte) 7);
        return pduHeader;
    }

    public EntityMarking getEntityMarking() {
        return entityMarking;
    }

    public void setEntityMarking(EntityMarking entityMarking) {
        this.entityMarking = entityMarking;
    }
    

    public EntityLocation getEntityLocation() {
        return entityLocation;
    }

    public void setEntityLocation(EntityLocation entityLocation) {
        this.entityLocation = entityLocation;
    }

    public EntityType getEntityType() {
        return entityType;
    }

    public void setEntityType(EntityType entityType) {
        this.entityType = entityType;
    }

    public EntityID getEntityID() {
        return entityID;
    }

    public void setEntityID(EntityID entityID) {
        this.entityID = entityID;
    }

    public byte getForceID() {
        return forceID;
    }

    public void setForceID(byte forceID) {
        this.forceID = forceID;
    }

    public EntityLinearVelocity getEntityLinearVelocity() {
        return entityLinearVelocity;
    }

    public void setEntityLinearVelocity(EntityLinearVelocity entityLinearVelocity) {
        this.entityLinearVelocity = entityLinearVelocity;
    }

    public EntityOrientation getEntityOrientation() {
        return entityOrientation;
    }

    public void setEntityOrientation(EntityOrientation entityOrientation) {
        this.entityOrientation = entityOrientation;
    }

    public Integer getEntityAppearance() {
        return entityAppearance;
    }

    public void setEntityAppearance(Integer entityAppearance) {
        this.entityAppearance = entityAppearance;
    }

    public DeadReckoningParameters getDeadReckoningParameters() {
        return deadReckoningParameters;
    }

    public void setDeadReckoningParameters(DeadReckoningParameters deadReckoningParameters) {
        this.deadReckoningParameters = deadReckoningParameters;
    }
    
    
}
