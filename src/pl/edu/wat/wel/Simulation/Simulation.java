/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.edu.wat.wel.Simulation;

import java.io.IOException;
import java.net.InetAddress;
import java.net.MulticastSocket;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import pl.edu.wat.wel.Threads.SocketThread;
import pl.edu.wel.Fields.EntityID;

/**
 *
 * @author Professional
 */
public class Simulation {
    
    private static final int PORT = 4444;
    private static final String ipAdress = "239.1.2.3";
    private byte protocolVersion;
    private byte exerciseID;
    private Date startTimestamp;
    private EntityID entityId;
    private static Simulation simulation;
    private SimulationEntity actualCreatingSimulationEntity;
    private ArrayList<SimulationEntity> connectedEntitiesList;
    private ArrayList<EntityID> connectEntityIdsList;
    private static int actualIdForEntity = 0;
    private static boolean simulationObjectCreated;
    private static boolean simulationCreated;
    private MulticastSocket socketReceiver;
    private static int actualRequestId;
    
    public static int getNextEntityNumber() {
        actualIdForEntity++;
        return actualIdForEntity;
    }

    public static int getActualRequestId() {
        return actualRequestId;
    }

    public static void setActualRequestId(int actualRequestId) {
        Simulation.actualRequestId = actualRequestId;
    }
    
    public ArrayList<String> getEntitiesNames() {
        ArrayList<String> list = new ArrayList<>();
        for (SimulationEntity se : connectedEntitiesList) {
            list.add(se.getMarkingWithId());
        }
        return list;
    }
    
    
    public void delete() {
        simulation = null;
        actualIdForEntity = 0;
        actualRequestId = 0;
        simulationCreated = false;
        simulationObjectCreated = false;
    }
    

    public static int getPORT() {
        return PORT;
    }

    public static String getIpAdress() {
        return ipAdress;
    }

    
    public static boolean isSimulationCreated() {
        return simulationCreated;
    }

    public static void setSimulationCreated(boolean simulationCreate) {
        Simulation.simulationCreated = simulationCreate;
    }
    

    public static boolean isSimulationObjectCreated() {
        return simulationObjectCreated;
    }

    public static void setSimulationObjectCreated(boolean seb) {
        simulationObjectCreated = seb;
    }
    
    

    public EntityID getEntityId() {
        return entityId;
    }

    public void setEntityId(EntityID entityId) {
        this.entityId = entityId;
    }
    


    public static int getActualIdForEntity() {
        return actualIdForEntity;
    }

    public static void setActualIdForEntity(int actualIdForEntity) {
        Simulation.actualIdForEntity = actualIdForEntity;
    }
    
    


    public ArrayList<SimulationEntity> getConnectedEntitiesList() {
        return connectedEntitiesList;
    }

    public void setConnectedEntitiesList(ArrayList<SimulationEntity> connectedEntitiesList) {
        this.connectedEntitiesList = connectedEntitiesList;
    }

    public ArrayList<EntityID> getConnectEntityIdsList() {
        return connectEntityIdsList;
    }

    public void setConnectEntityIdsList(ArrayList<EntityID> connectEntityIdsList) {
        this.connectEntityIdsList = connectEntityIdsList;
    }
    
    
    

    public byte getProtocolVersion() {
        return protocolVersion;
    }

    public void setProtocolVersion(byte protocolVersion) {
        this.protocolVersion = protocolVersion;
    }

    public byte getExerciseID() {
        return exerciseID;
    }

    public void setExerciseID(byte exerciseID) {
        this.exerciseID = exerciseID;
    }

    public Date getStartTimestamp() {
        return startTimestamp;
    }

    public void setStartTimestamp(Date startTimestamp) {
        this.startTimestamp = startTimestamp;
    }


    public SimulationEntity getActualCreatingSimulationEntity() {
        return actualCreatingSimulationEntity;
    }

    public void setActualCreatingSimulationEntity(SimulationEntity actualCreatingSimulationEntity) {
        this.actualCreatingSimulationEntity = actualCreatingSimulationEntity;
    }
    
    
    
    private Simulation() {
        protocolVersion = 7;
        startTimestamp = new Date();
        try {
            socketReceiver = new MulticastSocket(PORT);
            InetAddress adress =  InetAddress.getByName(ipAdress);
            socketReceiver.joinGroup(adress);
        } catch (IOException ex) {
            Logger.getLogger(Simulation.class.getName()).log(Level.SEVERE, null, ex);
        }
        actualCreatingSimulationEntity = new SimulationEntity();
        new SocketThread(socketReceiver).start();
        connectEntityIdsList = new ArrayList<>();
        connectedEntitiesList = new ArrayList<>();
        
    }
    
    public static Simulation getInstance() {
        if (simulation == null) {
            simulation = new Simulation();
        }
        //jakies dodatkowe dane reset
        return simulation;
    }
    
    
    /*Zadania na jutro
    *Skonfiguruj okno SimulationCreateForm z tą klasą
    *Zacznij dodawanie obiektu do symulacji
    */

    public void addToEntityIdsList(EntityID eid) {
        if (!entityIdexist(eid)) {
            connectEntityIdsList.add(eid);
        }
    }
    
    public boolean entityIdexist(EntityID eid) {
        for (EntityID entityId : connectEntityIdsList) {
            if (eid.equals(entityId))
                return true;
        }
        return false;
    }
    public boolean entityExist(EntityID eid) {
        for (SimulationEntity se : connectedEntitiesList) {
            if (eid.equals(se.getEntityID())) {
                return true;
            }
        }
        return false;
    }

    public void addToConnectedEntitiesList(SimulationEntity se) {
        if (!entityExist(se.getEntityID()))
        connectedEntitiesList.add(se);
    }

    public SimulationEntity searchInList(EntityID eid) {
        for (SimulationEntity se : connectedEntitiesList) {
            if (eid.equals(se.getEntityID())) {
                return se;
            }
        }
        return null;
    }
}
