/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.edu.wat.wel.creators;

import pl.edu.wat.wel.Simulation.Simulation;
import pl.edu.wat.wel.Simulation.SimulationEntity;
import pl.edu.wel.Fields.EntityLinearAcceleration;
import pl.edu.wel.Fields.EntityLinearVelocity;

/**
 *
 * @author Professional
 */
public class EntityVelocityCreator {
    
    private EntityLinearVelocity entityLinearVelocity;
    private Simulation sim;

    private void addToSim() {
        sim = Simulation.getInstance();
        SimulationEntity sm = sim.getActualCreatingSimulationEntity();
        sm.setEntityLinearVelocity(entityLinearVelocity);
        sm.setEntityAppearance(123115125);
        Simulation.setSimulationObjectCreated(true);        
    }

    public void createVelocity(float x, float y, float z) {
        this.entityLinearVelocity = new EntityLinearVelocity();
        this.entityLinearVelocity.setxELVComponent(x);
        this.entityLinearVelocity.setyELVComponent(y);
        this.entityLinearVelocity.setzELVComponent(z);
        addToSim();
    }

    public EntityLinearVelocity getEntityLinearVelocity() {
        return entityLinearVelocity;
    }

    public void setEntityVelocity(EntityLinearVelocity entityLinearVelocity) {
        this.entityLinearVelocity = entityLinearVelocity;
    }
}
