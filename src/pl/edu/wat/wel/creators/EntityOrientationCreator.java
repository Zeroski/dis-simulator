/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.edu.wat.wel.creators;

import pl.edu.wat.wel.Simulation.Simulation;
import pl.edu.wel.Fields.EntityOrientation;

/**
 *
 * @author Professional
 */
public class EntityOrientationCreator {
    
    
    private EntityOrientation entityOrientation;
    Simulation sim = Simulation.getInstance();

    private void addToSim() {
        sim.getActualCreatingSimulationEntity().setEntityOrientation(entityOrientation);

    }

    public void createOrientation(float psi, float theta, float phi) {
        this.entityOrientation = new EntityOrientation();
        this.entityOrientation.setPsi(psi);
        this.entityOrientation.setTheta(theta);
        this.entityOrientation.setPhi(phi);
        addToSim();
    }

    public EntityOrientation getEntityLocation() {
        return entityOrientation;
    }

    public void setEntityOrientation(EntityOrientation entityLocation) {
        this.entityOrientation = entityOrientation;
    }


}
