/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.edu.wat.wel.creators;

import pl.edu.wat.wel.Simulation.Simulation;
import pl.edu.wel.Fields.EntityLocation;

/**
 *
 * @author Professional
 */
public class LocationCreator {

    private EntityLocation entityLocation;
    Simulation sim = Simulation.getInstance();

    private void addToSim() {
        sim.getActualCreatingSimulationEntity().setEntityLocation(entityLocation);

    }

    public void createLocation(double x, double y, double z) {
        this.entityLocation = new EntityLocation();
        this.entityLocation.setxLComponent(x);
        this.entityLocation.setyLComponent(y);
        this.entityLocation.setzComponent(z);
        addToSim();
    }

    public EntityLocation getEntityLocation() {
        return entityLocation;
    }

    public void setEntityLocation(EntityLocation entityLocation) {
        this.entityLocation = entityLocation;
    }

}
//Utworzyć okno z informacjami o jednostce
//Algorytm Dead na póxniej