/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.edu.wat.wel.creators;

import java.util.ArrayList;
import java.util.List;
import pl.edu.wat.wel.Simulation.Simulation;
import pl.edu.wat.wel.entities.Enumerator;
import pl.edu.wat.wel.jdbc.OperacjeBazy;

/**
 *
 * @author Professional
 */
public class ForceIdCreator {
    
    private byte forceId;
    private Enumerator forceIdEnum;
    private List<Enumerator> listForceId;
    OperacjeBazy operacje;
    Simulation sim = Simulation.getInstance();
    
    private void addToSim() {
        sim.getActualCreatingSimulationEntity().setForceID(this.forceId);
    }
    
    public void createForceId() {
        this.forceId = (byte) (int) forceIdEnum.getEnumerator();
        addToSim();
    }
    
    public void setSelectedEnumerator(String s) {
        for (Enumerator e : listForceId) {
            if (s.equals(e.getNazwa())) {
                this.forceIdEnum = e;
            }
        }
    }
    
    public ForceIdCreator() {
        operacje = new OperacjeBazy();
        this.listForceId = operacje.pokazForceId();
    }
    
    public List<String> getForceIdNames() {
        List<String> list = new ArrayList<>();
        for (Enumerator e : this.listForceId) {
            list.add(e.getNazwa());
        }
        return list;
    }

    public List<Enumerator> getListForceId() {
        return listForceId;
    }

    public void setListForceId(List<Enumerator> listForceId) {
        this.listForceId = listForceId;
    }

    
    public byte getForceId() {
        return forceId;
    }

    public void setForceId(byte forceId) {
        this.forceId = forceId;
    }

    public Enumerator getForceIdEnum() {
        return forceIdEnum;
    }

    public void setForceIdEnum(Enumerator forceIdEnum) {
        this.forceIdEnum = forceIdEnum;
    }
    
    
}
