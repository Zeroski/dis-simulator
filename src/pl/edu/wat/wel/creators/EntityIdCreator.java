/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.edu.wat.wel.creators;

import pl.edu.wat.wel.Simulation.Simulation;
import pl.edu.wel.Fields.EntityID;

/**
 *
 * @author Professional
 */
public class EntityIdCreator {
    
    private EntityID entityID;
    private short SiteNumber;
    private short AppNumber;
    private short EntityNumber;
    private Simulation sim = Simulation.getInstance();
    public EntityID getEntityID() {
        return entityID;
    }

    public void setEntityID(EntityID entityID) {
        this.entityID = entityID;
    }

    public short getSiteNumber() {
        return SiteNumber;
    }

    public void setSiteNumber(short SiteNumber) {
        this.SiteNumber = SiteNumber;
    }

    public short getAppNumber() {
        return AppNumber;
    }

    public void setAppNumber(short AppNumber) {
        this.AppNumber = AppNumber;
    }

    public short getEntityNumber() {
        return EntityNumber;
    }

    public void setEntityNumber(short EntityNumber) {
        this.EntityNumber = EntityNumber;
    }
    
    
    public void createEntityId() {
        this.entityID = new EntityID();
        this.entityID.setEntityNumber(EntityNumber);
        this.entityID.setSiteNumber(SiteNumber);
        this.entityID.setAppNumber(AppNumber);
        addToSimulation();
    }
    private void addToSimulation() {
        sim.getActualCreatingSimulationEntity().setEntityID(entityID);
    }
}
