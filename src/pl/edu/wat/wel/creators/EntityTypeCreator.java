/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.edu.wat.wel.creators;

import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.ArrayList;
import java.util.List;
import pl.edu.wat.wel.Simulation.Simulation;
import pl.edu.wat.wel.Simulation.SimulationEntity;
import pl.edu.wat.wel.entities.Enumerator;
import pl.edu.wel.Fields.EntityType;
import pl.edu.wat.wel.jdbc.OperacjeBazy;

/**
 *
 * @author Professional
 */
public class EntityTypeCreator {

    private EntityType entityType;
    private List<Enumerator> listEK;
    private Enumerator enumeratorEK;
    private List<Enumerator> listDomain;
    private Enumerator enumeratorDomain;
    private List<Enumerator> listCountry;
    private Enumerator enumeratorCountry;
    private List<Enumerator> listCategory;
    private Enumerator enumeratorCategory;
    private List<Enumerator> listSubcategory;
    private Enumerator enumeratorSubcategory;
    private List<Enumerator> listSpecific;
    private Enumerator enumeratorSpecific;
    private List<Enumerator> listExtra;
    private Enumerator enumeratorExtra;
    private OperacjeBazy operacjeBazy;
    private Simulation sim = Simulation.getInstance();
    
    private void addToSimulation() {
        sim.setActualCreatingSimulationEntity(new SimulationEntity());
        sim.getActualCreatingSimulationEntity().setEntityType(entityType);
    }

    public void createEntiyType() {
        this.entityType = new EntityType();
        this.entityType.setEntityKind((byte) (int) enumeratorEK.getEnumerator());
        if (enumeratorDomain != null) {
            this.entityType.setDomain((byte) (int) enumeratorDomain.getEnumerator());
        }
        if (enumeratorCountry != null) {
            this.entityType.setCountry((byte) (int) enumeratorCountry.getEnumerator());
        }
        if (enumeratorCategory != null) {
            this.entityType.setCategory((byte) (int) enumeratorCategory.getEnumerator());
        }
        if (enumeratorSubcategory != null) {
            this.entityType.setSubcategory((byte) (int) enumeratorSubcategory.getEnumerator());
        }
        if (enumeratorSpecific != null) {
            this.entityType.setSpecific((byte) (int) enumeratorSpecific.getEnumerator());
        }
        if (enumeratorExtra != null) {
            this.entityType.setExtra((byte) (int) enumeratorExtra.getEnumerator());
        }
        addToSimulation();
    }

    public void setEnumeratorEK(Enumerator enumeratorEK) {
        this.enumeratorEK = enumeratorEK;
    }

    public void setEnumeratorDomain(Enumerator enumeratorDomain) {
        this.enumeratorDomain = enumeratorDomain;
    }

    public void setEnumeratorCountry(Enumerator enumeratorCountry) {
        this.enumeratorCountry = enumeratorCountry;
    }

    public void setEnumeratorCategory(Enumerator enumeratorCategory) {
        this.enumeratorCategory = enumeratorCategory;
    }

    public void setEnumeratorSubcategory(Enumerator enumeratorSubcategory) {
        this.enumeratorSubcategory = enumeratorSubcategory;
    }

    public void setEnumeratorSpecific(Enumerator enumeratorSpecific) {
        this.enumeratorSpecific = enumeratorSpecific;
    }

    public void setEnumeratorExtra(Enumerator enumeratorExtra) {
        this.enumeratorExtra = enumeratorExtra;
    }

    public List<Enumerator> getListSpecific() {
        return listSpecific;
    }

    public void setListSpecific() {
        this.listSpecific = operacjeBazy.pokazSpecyfikacje(enumeratorSubcategory.getId());
    }

    public Enumerator getEnumeratorSpecific() {
        return enumeratorSpecific;
    }

    public void setEnumeratorSpecific(String nazwa) {
        this.enumeratorSpecific = searchEnumeratorFromtList(listSpecific, nazwa);
    }

    public List<Enumerator> getListExtra() {
        return listExtra;
    }

    public void setListExtra() {
        this.listExtra = operacjeBazy.pokazExtra(enumeratorCategory.getId());
    }

    public Enumerator getEnumeratorExtra() {
        return enumeratorExtra;
    }

    public void setEnumeratorExtra(String nazwa) {
        this.enumeratorExtra = searchEnumeratorFromtList(listExtra, nazwa);
    }

    public List<Enumerator> getListSubcategory() {
        return listSubcategory;
    }

    public void setListSubcategory() {
        this.listSubcategory = operacjeBazy.pokazPodkategorie(enumeratorEK.getId(), enumeratorDomain.getId(), enumeratorCategory.getId());
    }

    public Enumerator getEnumeratorSubcategory() {
        return enumeratorSubcategory;
    }

    public void setEnumeratorSubcategory(String nazwa) {
        this.enumeratorSubcategory = searchEnumeratorFromtList(listSubcategory, nazwa);
    }

    public Enumerator getEnumeratorCategory() {
        return enumeratorCategory;
    }

    public void setEnumeratorCategory(String nazwa) {
        this.enumeratorCategory = searchEnumeratorFromtList(listCategory, nazwa);
    }

    public Enumerator getEnumeratorCountry() {
        return enumeratorCountry;
    }

    public void setEnumeratorCountry(String nazwa) {
        this.enumeratorCountry = searchEnumeratorFromtList(listCountry, nazwa);
    }

    public Enumerator getEnumeratorDomain() {
        return enumeratorDomain;
    }

    public void setEnumeratorDomain(String nazwa) {
        this.enumeratorDomain = searchEnumeratorFromtList(listDomain, nazwa);
    }

    private Enumerator searchEnumeratorFromtList(List<Enumerator> list, String nazwa) {
        for (Enumerator e : list) {
            if (nazwa.equals(e.getNazwa())) {
                return e;
            }
        }
        return null;
    }

    public Enumerator getEnumeratorEK() {
        return enumeratorEK;
    }

    public void setEnumeratorEK(String nazwa) {
        this.enumeratorEK = searchEnumeratorFromtList(listEK, nazwa);
    }

    public List<Enumerator> getListCategory() {
        return listCategory;
    }

    public void setListCategory() {
        this.listCategory = operacjeBazy.pokazKategorie(enumeratorEK.getId(), enumeratorDomain.getId());
    }

    public List<Enumerator> getListCountry() {
        return listCountry;
    }

    public void setListCountry() {
        this.listCountry = operacjeBazy.pokazKraje(this.enumeratorEK.getId());
    }

    public void setListCountry(List<Enumerator> listCountry) {
        this.listCountry = listCountry;
    }

    public List<Enumerator> getListEK() {
        return listEK;
    }

    public void setListEK(List<Enumerator> listEK) {
        this.listEK = listEK;
    }

    public List<Enumerator> getListDomain() {
        return listDomain;
    }

    public void setListDomain() {
        this.listDomain = operacjeBazy.pokazDomeny(enumeratorEK.getId());

    }

    public EntityTypeCreator() {
        this.operacjeBazy = new OperacjeBazy();
        setListEK(operacjeBazy.pokazWszystkieEntityKind());
    }

    public Integer getEnumeratorEK(String name) {
        for (Enumerator e : this.listEK) {
            if (name.equals(e.getNazwa())) {
                return e.getEnumerator();
            }
        }
        return null;
    }

    public Integer getEnumeratorDomain(String name) {
        for (Enumerator e : this.listDomain) {
            if (name.equals(e.getNazwa())) {
                return e.getEnumerator();
            }
        }
        return null;
    }

    public Integer getEnumeratorCountry(String name) {
        for (Enumerator e : this.listCountry) {
            if (name.equals(e.getNazwa())) {
                return e.getEnumerator();
            }
        }
        return null;
    }

    public List<String> getEKNames() {
        List<String> listEKNames = new ArrayList();
        if (this.listEK == null) {
            return null;
        }
        for (Enumerator e : listEK) {
            listEKNames.add(e.getNazwa());
        }
        return listEKNames;
    }

    public List<String> getDomainNames() {
        List<String> listDomainNames = new ArrayList();
        if (this.listDomain == null) {
            return null;
        }
        for (Enumerator e : listDomain) {
            listDomainNames.add(e.getNazwa());
        }
        return listDomainNames;
    }

    public List<String> getCountryNames() {
        List<String> listCountryNames = new ArrayList();
        if (this.listCountry == null) {
            return null;
        }
        for (Enumerator e : listCountry) {
            listCountryNames.add(e.getNazwa());
        }
        return listCountryNames;
    }

    public List<String> getCategoryNames() {
        List<String> listCategoryNames = new ArrayList();
        if (this.listCategory == null) {
            return null;
        }
        for (Enumerator e : listCategory) {
            listCategoryNames.add(e.getNazwa());
        }
        return listCategoryNames;
    }

    public List<String> getSubcategoryNames() {
        List<String> listSubcategoryNames = new ArrayList();
        if (this.listSubcategory == null) {
            return null;
        }
        for (Enumerator e : listSubcategory) {
            listSubcategoryNames.add(e.getNazwa());
        }
        return listSubcategoryNames;
    }

    public List<String> getSpecificNames() {
        List<String> listSpecificNames = new ArrayList();
        if (this.listSpecific == null) {
            return null;
        }
        for (Enumerator e : listSpecific) {
            listSpecificNames.add(e.getNazwa());
        }
        return listSpecificNames;
    }

    public List<String> getExtraNames() {
        List<String> listExtraNames = new ArrayList();
        if (this.listExtra == null) {
            return null;
        }
        for (Enumerator e : listExtra) {
            listExtraNames.add(e.getNazwa());
        }
        return listExtraNames;
    }

    public EntityType getEntityType() {
        return entityType;
    }

    public void setEntityType(EntityType entityType) {
        this.entityType = entityType;
    }

}
